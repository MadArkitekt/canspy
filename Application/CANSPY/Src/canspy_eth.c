/**
  ******************************************************************************
  * @file    canspy_eth.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   ETH module core.
  *          This file provides all the functions needed by ETH services:
  *           + Frame crafting
  *           + Tapping
  *           + Bridging
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#include "canspy_eth.h"
#include "canspy_can.h"
#include "canspy_shell.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  Strings associated with the acknowledgement options.
  */
char *CANSPY_ETH_ACK_String[] = {"NONE", "ECHO", "CMD"};

/** @brief  Array of EtherTypes.
  */
uint16_t CANSPY_ETH_Type[ETH_IF + 1] = {0x88b6, 0x88b5, 0x88b5, 0x88b6};

/** @brief  Array of MAC addresses.
  */
uint8_t CANSPY_ETH_Mac[ETH_IF + 1][6] = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
																				 {0x00, 0x80, 0xE1, 0x00, 0x00, CAN1_IF},
																				 {0x00, 0x80, 0xE1, 0x00, 0x00, CAN2_IF},
																				 {0x00, 0x80, 0xE1, 0x00, 0x00, 0x00}};

/** @brief  Specifies if commands received over Ethernet should return their output.
  */
bool CANSPY_ETH_CMD_Output = false;

/**
  * @brief  The ETH device handler.
  * @param  call: the call to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_eth_device(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt){

	if(call == DEV_INIT){

		CANSPY_DEVICE_OPTION("eth_mac", CANSPY_OPTION_TYPES_Range[OPTION_MACADDR], CANSPY_OPTION_TYPES_Length[OPTION_MACADDR],
			CANSPY_ETH_Mac[ETH_IF], "The Ethernet MAC address of this device", INSTANT);
		CANSPY_DEVICE_OPTION("eth_dbg", CANSPY_OPTION_TYPES_Range[OPTION_DEBUG], CANSPY_OPTION_TYPES_Length[OPTION_DEBUG],
			&CANSPY_DEBUG_ETH_Print, "Specifies the level of debug messages that should be sent over Ethernet", INSTANT);
	}
	else if(call == DEV_START){

		if(MX_ETH_Init() != HAL_OK){

			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to init ETH HAL");
			return EXIT_FAILURE;
		}

		/* Make MAC addresses unique based on MCU ID at 0x1FFF7A10 */
		CANSPY_ETH_Mac[CAN1_IF][3] = CANSPY_ETH_Mac[CAN2_IF][3] = CANSPY_ETH_Mac[ETH_IF][3] = ((uint8_t* )0x1FFF7A10)[0];
		CANSPY_ETH_Mac[CAN1_IF][4] = CANSPY_ETH_Mac[CAN2_IF][4] = CANSPY_ETH_Mac[ETH_IF][4] = ((uint8_t* )0x1FFF7A10)[2];

		ETH_DMA_Tx_Desc->ControlBufferSize = 100;
		ETH_DMA_Rx_Desc->ControlBufferSize = ETH_MAX_PACKET_SIZE | (1 << 14);
		ETH_DMA_Rx_Desc->Status = ETH_DMARXDESC_OWN;

		if(HAL_ETH_DMATxDescListInit(&heth, ETH_DMA_Tx_Desc, &ETH_Tx_Buf[0][0], ETH_TXBUFNB) != HAL_OK){

			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to init ETH-Tx DMA");
			return EXIT_FAILURE;
		}

		if(HAL_ETH_DMARxDescListInit(&heth, ETH_DMA_Rx_Desc, &ETH_Rx_Buf[0][0], ETH_RXBUFNB != HAL_OK)){

			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to init ETH-Rx DMA");
			return EXIT_FAILURE;
		}

		while(HAL_ETH_Start(&heth) != HAL_OK);
	}
	else if(call == DEV_STOP){

		while(HAL_ETH_Stop(&heth) != HAL_OK);

		if(HAL_ETH_DeInit(&heth) != HAL_OK){

			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to deinit ETH HAL");
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_GET){

		if(CANSPY_OPTION_CMP(CANSPY_ETH_Mac[ETH_IF]))
			CANSPY_OPTION_GET(macaddr, CANSPY_ETH_Mac[ETH_IF]);
		else if(CANSPY_OPTION_CMP(&CANSPY_DEBUG_ETH_Print))
			CANSPY_OPTION_GET(debug, &CANSPY_DEBUG_ETH_Print);
		else{

			CANSPY_DEBUG_ERROR_1(WARN, CANSPY_OPTION_ERROR_Unknown, CANSPY_OPTION_VAR);
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_SET){

		if(CANSPY_OPTION_CMP(CANSPY_ETH_Mac[ETH_IF]))
			CANSPY_OPTION_SET(macaddr, CANSPY_ETH_Mac[ETH_IF]);
		else if(CANSPY_OPTION_CMP(&CANSPY_DEBUG_ETH_Print))
			CANSPY_OPTION_SET(debug, &CANSPY_DEBUG_ETH_Print);
		else{

			CANSPY_DEBUG_ERROR_1(WARN, CANSPY_OPTION_ERROR_Unknown, CANSPY_OPTION_VAR);
			return EXIT_FAILURE;
		}
	}
	else{

		CANSPY_DEBUG_ERROR_1(WARN, CANSPY_DEVICE_ERROR_Unknown, call);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/**
  * @brief  Defined as ETH_Rx_Handler.
  * @param  seg_count: the segment count.
  * @retval Handled code
  */
int canspy_eth_handler(uint8_t seg_count){

	CANSPY_SCHED_SIGNAL(ETH_RX);

	return 0;
}

/**
  * @brief  Wrap calls to htons and ntohs.
  * @param  reverse: calls ntohs.
  * @retval Byte-ordered short
  */
uint16_t canspy_eth_short(uint16_t val, bool reverse){

	if(reverse)
		return ntohs(val);
	else
		return htons(val);
}

/**
  * @brief  Send a frame on the Ethernet device.
  * @param  buffer: the buffer to use.
  * @param  data: the data to send.
  * @param  size: the size of the data.
  * @param  eth_mac_src: the src MAC address.
  * @param  eth_mac_dst: the dst MAC address.
  * @param  type: the EtherType.
  * @param  forge: do not send, just forge the frame.
  * @retval Exit Status
  */
int canspy_eth_send(void *buffer, const void *data, unsigned int size, uint8_t *eth_mac_src, uint8_t *eth_mac_dst, uint16_t type, bool forge){

	HAL_StatusTypeDef res;
	uint8_t *tmp_buf = buffer;

	/* Setting default MAC addresses */
	if(eth_mac_src == NULL)
		eth_mac_src = ETH_Mac_Src;
	if(eth_mac_dst == NULL)
		eth_mac_src = ETH_Mac_Dst;

	/* Setting dst MAC address */
	memcpy(tmp_buf, eth_mac_dst, sizeof(ETH_Mac_Dst));

	/* Setting src MAC address */
	tmp_buf += sizeof(ETH_Mac_Dst);
	memcpy(tmp_buf, eth_mac_src, sizeof(ETH_Mac_Src));
	memcpy(ETH_Mac_Src, eth_mac_src, sizeof(ETH_Mac_Src));

	/* Setting EtherType */
	tmp_buf += sizeof(ETH_Mac_Src);
	*(uint16_t *)tmp_buf = htons(type);

	/* Copying data */
	tmp_buf += sizeof(uint16_t);
	memcpy(tmp_buf, data, size);

	if(!forge){

		if(!CANSPY_DEVICE_ACTIVE(CANSPY_ETH))
			return EXIT_FAILURE;

		while((res = HAL_ETH_TransmitFrame(&heth, size + CANSPY_ETH_HEADER_SIZE)) == HAL_BUSY);

		if(res != HAL_OK){

			//CANSPY_DEBUG_ERROR_1(ERROR, "Failed to transmit ETH frames: %i", res);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

/**
  * @brief  Receive a frame on the Ethernet device.
  * @param  buffer: the buffer to use.
  * @param  data: the data to receive.
  * @param  size: the size of the data.
  * @param  eth_mac_dst: the dst MAC address.
  * @param  eth_mac_src: the src MAC address.
  * @retval EtherType
  */
uint16_t canspy_eth_recv(void *buffer, void *data, unsigned int size, uint8_t *eth_mac_dst, uint8_t *eth_mac_src){

	uint16_t type;
	uint8_t *tmp_buf = buffer;

	/* Copying dst MAC address */
	if(eth_mac_dst != NULL)
		memcpy(eth_mac_dst, tmp_buf, sizeof(ETH_Mac_Dst));

	/* Copying src MAC address */
	tmp_buf += sizeof(ETH_Mac_Dst);
	if(eth_mac_src != NULL)
		memcpy(eth_mac_src, tmp_buf, sizeof(ETH_Mac_Src));

	/* Copying EtherType */
	tmp_buf += sizeof(ETH_Mac_Src);
	type = ntohs(*(uint16_t *)tmp_buf);

	/* Copying data (ignoring the header)*/
	tmp_buf += sizeof(uint16_t);
	if(data != NULL && size != 0)
		memcpy(data, tmp_buf, size);

	/* Returning the EtherType */
	return type;
}

/**
  * @brief  The bridge service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_eth_bridge(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	CanRxMsgTypeDef *can_rx;
	SOCKETCAN can_frame;
	uint16_t ether_type;
	uint8_t dst_mac[6];

	if(flag == SVC_INIT){

		CANSPY_DEVICE_OPTION("bri_ack", CANSPY_OPTION_TYPES_Range[OPTION_ETHACK], CANSPY_OPTION_TYPES_Length[OPTION_ETHACK],
			&CANSPY_INJ_ETH_Ack, "Specifies how to acknowledge CAN frame injection", INSTANT);
	}
	else if(flag == SVC_GET){

		if(CANSPY_OPTION_CMP(&CANSPY_INJ_ETH_Ack))
			CANSPY_OPTION_GET(ethack, &CANSPY_INJ_ETH_Ack);
	}
	else if(flag == SVC_SET){

		if(CANSPY_OPTION_CMP(&CANSPY_INJ_ETH_Ack))
			CANSPY_OPTION_SET(ethack, &CANSPY_INJ_ETH_Ack);
	}
	else if(flag == CAN1_RX || flag == CAN2_RX ){

		can_rx = canspy_sched_buffer(flag);
		canspy_can_wrap(&can_frame, can_rx, true);
		canspy_eth_send(ETH_Tx_Buf[0], &can_frame, sizeof(can_frame), CANSPY_ETH_Mac[flag], CANSPY_ETH_Mac[ETH_BROADCAST], CANSPY_ETH_Type[flag], false);
	}
	else if(flag == ETH_RX){

		ether_type = canspy_eth_recv(canspy_sched_buffer(flag), &CANSPY_INJ_ETH_Sock, sizeof(CANSPY_INJ_ETH_Sock), dst_mac, NULL);

		if(ether_type == CANSPY_ETH_Type[CAN1_RX] && memcmp(dst_mac, CANSPY_ETH_Mac[CAN1_RX], sizeof(dst_mac)) == 0){

			CANSPY_INJ_ETH_If = CAN1_IF;
			CANSPY_SCHED_SIGNAL(INJ_ETH);
		}
		else if(ether_type == CANSPY_ETH_Type[CAN2_RX] && memcmp(dst_mac, CANSPY_ETH_Mac[CAN2_RX], sizeof(dst_mac)) == 0){

			CANSPY_INJ_ETH_If = CAN2_IF;
			CANSPY_SCHED_SIGNAL(INJ_ETH);
		}
	}

	return EXIT_SUCCESS;
}

/**
  * @brief  The wiretap service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_eth_wiretap(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	CanRxMsgTypeDef *can_rx;
	SOCKETCAN can_frame;

	if(flag == CAN1_RX || flag == CAN2_RX ){

		can_rx = canspy_sched_buffer(flag);
		canspy_can_wrap(&can_frame, can_rx, true);
		canspy_eth_send(ETH_Tx_Buf[0], &can_frame, sizeof(can_frame), CANSPY_ETH_Mac[flag], CANSPY_ETH_Mac[ETH_BROADCAST], CANSPY_ETH_Type[flag], false);
	}

	return EXIT_SUCCESS;
}

/**
  * @brief  The command service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_eth_command(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	uint16_t ether_type;
	uint8_t dst_mac[6];

	if(flag == SVC_INIT){

		CANSPY_SERVICE_OPTION("cmd_typ", CANSPY_OPTION_TYPES_Range[OPTION_ETHTYPE], CANSPY_OPTION_TYPES_Length[OPTION_ETHTYPE],
			&(CANSPY_ETH_Type[ETH_IF]), "The EtherType used for commands over Ethernet", INSTANT);
		CANSPY_SERVICE_OPTION("cmd_out", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&CANSPY_ETH_CMD_Output, "Specifies if commands received over Ethernet should return their output", INSTANT);
	}
	else if(flag == SVC_GET){

		if(CANSPY_OPTION_CMP(&CANSPY_ETH_CMD_Output))
			CANSPY_OPTION_GET(bool, &CANSPY_ETH_CMD_Output);
		else if(CANSPY_OPTION_CMP(&(CANSPY_ETH_Type[ETH_IF])))
			CANSPY_OPTION_GET(ethtype, &(CANSPY_ETH_Type[ETH_IF]));
	}
	else if(flag == SVC_SET){

		if(CANSPY_OPTION_CMP(&CANSPY_ETH_CMD_Output))
			CANSPY_OPTION_SET(bool, &CANSPY_ETH_CMD_Output);
		else if(CANSPY_OPTION_CMP(&(CANSPY_ETH_Type[ETH_IF])))
			CANSPY_OPTION_SET(ethtype, &(CANSPY_ETH_Type[ETH_IF]));
	}
	else if(flag == ETH_RX){

		static char cmd_buf[UART_LINE_RX_BUF_SIZE];
		ether_type = canspy_eth_recv(canspy_sched_buffer(flag), cmd_buf, sizeof(cmd_buf), dst_mac, NULL);

		if(ether_type == CANSPY_ETH_Type[ETH_IF]){

			if(memcmp(dst_mac, CANSPY_ETH_Mac[ETH_IF], sizeof(dst_mac)) == 0 || memcmp(dst_mac, CANSPY_ETH_Mac[ETH_BROADCAST], sizeof(dst_mac)) == 0){

				canspy_device_pause(CANSPY_DEVICE_POINTER(CANSPY_UART));
				canspy_shell_execute(cmd_buf);
				canspy_device_resume(CANSPY_DEVICE_POINTER(CANSPY_UART));
			}
		}
	}

	return EXIT_SUCCESS;
}

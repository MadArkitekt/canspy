/**
  ******************************************************************************
  * @file    canspy_device.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Device module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#ifndef __CANSPY_DEVICE_H
#define __CANSPY_DEVICE_H

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_can.h"
#include "stm32f4xx_eth.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_bsp_driver_sd.h"
#include "canspy_option.h"

/* External typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_SERVICE_FLAG CANSPY_SERVICE_FLAG;

/* Exported typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_DEVICE_ID{

	CANSPY_NODEVICE = -1,
	CANSPY_UART = 0,
	CANSPY_CAN1 = CAN1_IF,
	CANSPY_CAN2 = CAN2_IF,
	CANSPY_ETH,
	CANSPY_SDCARD,
	CANSPY_DEVICES
}
CANSPY_DEVICE_ID;

typedef enum _CANSPY_DEVICE_CALL{

	DEV_SET = OPT_SET,
	DEV_GET = OPT_GET,
	DEV_STOP = OPT_STOP,
	DEV_START = OPT_START,
	DEV_POST = OPT_STOP,
	DEV_INIT = OPT_INIT,
}
CANSPY_DEVICE_CALL;

typedef int (*CANSPY_DEVICE_FUNC)(CANSPY_DEVICE_CALL, CANSPY_OPTION_CALL *);

typedef struct _CANSPY_DEVICE_DESC{

	CANSPY_DEVICE_ID dev_id;
	CANSPY_OPTION_DESC *dev_opt;
	CANSPY_DEVICE_FUNC dev_func;
	const char *dev_name;
	void *dev_handle;
	bool powered;
	bool paused;
	CANSPY_SERVICE_DESC *svc_list;
	int svc_length;
	int svc_sync;
	int svc_async;
}
CANSPY_DEVICE_DESC;

/* Exported constants --------------------------------------------------------*/
#define CANSPY_DEVICE_FIRST ((CANSPY_DEVICE_ID)0)

/* Exported globals ----------------------------------------------------------*/
extern char *CANSPY_DEVICE_ERROR_Unknown;

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_DEVICE_POINTER(id) (&(CANSPY_SCHED_Devices[(id)]))
#define CANSPY_DEVICE_ACTIVE(d) ((CANSPY_DEVICE_POINTER((d))->powered) && (!(CANSPY_DEVICE_POINTER((d))->paused)))
#define CANSPY_DEVICE_OPTION(NAME, RANGE, SIZE, ADDR, DESC, RESTART) canspy_option_register(opt->dev_ptr->dev_opt, NAME, ADDR, RANGE, DESC, SIZE, RESTART)
#define CANSPY_DEVICE_DEBUG() CANSPY_DEBUG_DEVICE((CANSPY_DEVICE_DESC *)(opt->dev_ptr))

/* Exported functions --------------------------------------------------------*/
int canspy_device_register(CANSPY_DEVICE_ID dev_id, CANSPY_DEVICE_FUNC dev_func, const char *dev_name, void *dev_handle, bool power);
int canspy_device_start(CANSPY_DEVICE_DESC *dev_ptr);
int canspy_device_pause(CANSPY_DEVICE_DESC *dev_ptr);
int canspy_device_resume(CANSPY_DEVICE_DESC *dev_ptr);
int canspy_device_stop(CANSPY_DEVICE_DESC *dev_ptr);
CANSPY_DEVICE_DESC *canspy_device_walk(CANSPY_DEVICE_DESC *cur_dev);
CANSPY_DEVICE_DESC *canspy_device_find(CANSPY_DEVICE_ID dev_id, const char *dev_name, CANSPY_DEVICE_FUNC dev_func);
CANSPY_SERVICE_DESC *canspy_device_active(CANSPY_DEVICE_DESC *dev_ptr, CANSPY_SERVICE_DESC *cur_svc);

#endif /*_CANSPY_DEVICE_H*/
